import csv
import gc
import sys
import pandas as pd
from sklearn.ensemble import bart as bart
import numpy as np
import time
from sklearn.model_selection import train_test_split
import argparse
import random


# Main uncertain trees
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Welcome to TDT builder')
    parser.add_argument('-x', type=str, help='x path')
    parser.add_argument('-o', type=str, help='output file')
    parser.add_argument('-e', type=str, help='error file')
    parser.add_argument('-n', type=int, help='number of estimator', default=100)
    parser.add_argument('-i', type=int, help='number of burn-in iteration', default=50)
    parser.add_argument('-b', type=int, help='number of after burn-in iteration', default=200)
    parser.add_argument('-l', type=float, help='min leaf percentage', default=0.05)
    parser.add_argument('-sg', type=str, help='sigma values separated by , for each fold', default=None)
    parser.add_argument('-cvs', type=int, help='cross validation min/max range', default=0)
    parser.add_argument('-cve', type=int, help='cross validation max range', default=10)
    parser.add_argument('-ts', type=float, help='test size in percentage', default=0.2)
    args = parser.parse_args()

    X = pd.read_csv(args.x, header=None, index_col=None)
    X = X.values
    Y = X[:, -1]
    X = X[:, 0:X.shape[1] - 1]

    outputs = []
    times = []
    rmses = []
    all_errors = []
    all_predictions = []
    all_y = []

    yMaxMinHalved = 0.5

    for ind, k in enumerate(range(args.cvs, args.cve)):

        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=args.ts, random_state=k)

        ############################################################################################
        # Normalizing Y to be in (-0.5, 0.5)
        min_y = np.min(y_train)
        max_y = np.max(y_train)
        subtraction = max_y - min_y
        y_train -= min_y
        y_train = np.array([x / subtraction for x in y_train], dtype=np.float64)
        y_train -= yMaxMinHalved

        ############################################################################################

        print('Cross Validation:', k)
        min_samples_leaf = round(len(X_train) * args.l)
        random_seed = random.randint(0, 10000)
        print('Random seed', random_seed)
        best_sigma = 1e-15
        regressor = bart.BARTRegressor(random_state=random_seed, n_estimators=args.n,
                                       presort=False,  min_samples_leaf=min_samples_leaf,
                                       sigma_Xp=best_sigma,
                                       n_iteration=args.i, n_after_burn_iteration=args.b, criterion='mseprob')
        t = time.process_time()
        regressor.fit(X_train, y_train)
        t = time.process_time() - t
        prediction = regressor.predict(X_test)

        ##########################################################################################
        prediction += yMaxMinHalved
        prediction *= subtraction
        prediction += min_y
        ##########################################################################################
        all_predictions.extend(prediction)
        all_y.extend(y_test)
        error = abs(y_test - prediction)
        all_errors.extend(error)
        RMSE_test = np.sqrt(np.mean(error ** 2))
        print(RMSE_test)
        outputs.append([k, RMSE_test, t, best_sigma, random_seed])

        times.append(t)
        rmses.append(RMSE_test)

    print(rmses)
    all_errors = np.array(all_errors)
    outputs.append(['Average MSE', np.mean(rmses)])
    outputs.append(['STD', np.std(rmses)])
    print('Average MSE', np.mean(rmses))
    print('Average Std', np.std(rmses))
    outputs.append(['Average Time', np.mean(times)])
    outputs.append(['Total Average MSE', np.sqrt(np.mean(all_errors ** 2))])
    gc.collect()

